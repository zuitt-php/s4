<?php

//public class can be accessible for modifying, inheritance, polymorph

//private class cannot be accessible by inheritance or polymorph

//protected also disables direct access to an object's properties and methods but inheritance is allowed.

//Access modifiers is inherited and cannot be modified

class Building {
	
	/*public $name;
	public $floors;
	public $address;*/

	
	/*private $name;
	private $floors;
	private $address;*/


	protected $name;
	protected $floors;
	protected $address;


	public function __construct($name1, $floors1, $address1){
		$this->name = $name1;
		$this->floors = $floors1;
		$this->address = $address1;
	}
}

//[Encapsulation]

/*
Getters and setters applies on both private and protected properties
*/

class Condominium extends Building {
	public function getName(){
		return $this->name;
	}
	public function setName($name1){
		$this->name = $name1;
	}
}

$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');

// $building->name = 'Change Name';
$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');

