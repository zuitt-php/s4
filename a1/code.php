<?php

class Building{

	protected $name;
	protected $floors;
	protected $address;

	public function __construct($name1, $floors1, $address1){
		$this->name = $name1;
		$this->floors = $floors1;
		$this->address = $address1;
	}

	/*----- Getters ------*/

	public function getName(){
		return $this->name;
	}
	public function getFloors(){
		return $this->floors;
	}
	public function getAddress(){
		return $this->address;
	}

	/*----- Setters ------*/

	public function setName($nameA){
		$this->name = $nameA;
	}

}

class Condominium extends Building {

	/*----- Getters ------*/

	public function getName(){
		return $this->name;
	}

	/*----- Setters ------*/

	public function setName($nameB){
		$this->name = $nameB;
	}
}

$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');
$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');