<?php require_once './code.php'; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Activity 4</title>
</head>
<body>


<h3>Building</h3>

<!-- Encapsulation -->
<p>
	The name of the building is
	<?php echo $building->getName(); ?>.
</p>

<p>
	The
	<?php echo $building->getName(); ?>
	has
	<?php echo $building->getFloors(); ?>
	floors.
</p>

<p>
	The
	<?php echo $building->getName(); ?>
	is located at
	<?php echo $building->getAddress(); ?>.
</p>

<p>
	The name of the building has been changed to
	<?= $building->setName('Caswynn Complex'); ?>
	<?= $building->getName(); ?>.
</p>

<!-- ---------------------------------------------------------------------- -->

<h3>Condominium</h3>

<!-- Encapsulation -->
<p>
	The name of the condominium is
	<?php echo $condominium->getName(); ?>.
</p>

<p>
	The
	<?php echo $condominium->getName(); ?>
	has
	<?php echo $condominium->getFloors(); ?>
	floors.
</p>

<p>
	The
	<?php echo $condominium->getName(); ?>
	is located at
	<?php echo $condominium->getAddress(); ?>
</p>

<p>
	The name of the condominium has been changed to
	<?= $condominium->setName('Caswynn Complex'); ?>
	<?= $condominium->getName(); ?>.
</p>

</body>
</html>